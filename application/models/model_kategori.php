<?php
class Model_kategori extends CI_Model
{
    public function data_hewan()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Hewan'));
    }
    public function data_wayang()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Wayang'));
    }
    public function data_wanita()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Wanita'));
    }
    public function data_anak()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Anak'));
    }
    public function data_lain()
    {
        return $this->db->get_where('tb_barang', array('kategori' => 'Lain'));
    }
}
