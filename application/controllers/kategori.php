<?php
class Kategori extends CI_Controller
{
    public function hewan()
    {
        $data['hewan'] = $this->model_kategori->data_hewan()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('hewan', $data);
        $this->load->view('templates/footer');
    }
    public function wayang()
    {
        $data['wayang'] = $this->model_kategori->data_wayang()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('wayang', $data);
        $this->load->view('templates/footer');
    }
    public function wanita()
    {
        $data['wanita'] = $this->model_kategori->data_wanita()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('wanita', $data);
        $this->load->view('templates/footer');
    }
    public function sepatu_anak()
    {
        $data['anak'] = $this->model_kategori->data_anak()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('anak', $data);
        $this->load->view('templates/footer');
    }
    public function peralatan_olahraga()
    {
        $data['lain'] = $this->model_kategori->data_lain()->result();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('lain', $data);
        $this->load->view('templates/footer');
    }
}
